//go:build !js

// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0

// iban analyses an International Bank Account Number (IBAN). Only for JS+wasm.
package main
