//go:build js

// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0

package main

import (
	"fmt"
	"syscall/js"

	"code.pfad.fr/swift"
	"code.pfad.fr/swift/bic"
)

func main() {
	js.Global().Set("swift", make(map[string]interface{}))
	module := js.Global().Get("swift")
	module.Set("NewIBAN",
		js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			if len(args) != 1 {
				return map[string]interface{}{
					"error": fmt.Sprintf("expected exactly one argument, got: %d", len(args)),
				}
			}

			iban, err := swift.NewIBAN(args[0].String(), swift.NationalFormatCheck())
			bic := bic.FromIBAN(iban)
			res := map[string]interface{}{
				"country": map[string]interface{}{
					"code":        string(iban.CountryCode),
					"is_registry": iban.CountryCode.BelongsToIBANRegistry(),
					"is_sepa":     iban.CountryCode.BelongsToSEPA(),
				},
				"check_digits": iban.CheckDigits,
				"bban":         iban.BBAN,
				"iban": map[string]interface{}{
					"spaced": iban.Spaced(),
					"string": iban.String(),
					"masked": iban.MaskedAndSpaced(),
				},
				"bic": bic.String(),
			}
			if err != nil {
				res["error"] = err.Error()
			}

			return res
		}),
	)

	<-make(chan struct{})
}
