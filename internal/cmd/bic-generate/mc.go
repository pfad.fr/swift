// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"fmt"
	"io"
	"os"
	"regexp"

	"code.pfad.fr/swift"
)

func parseMChtml(htmlFile *os.File) (map[string]swift.BIC, error) {
	buf, err := io.ReadAll(htmlFile)
	if err != nil {
		return nil, err
	}

	matches := regexp.MustCompile(`<td>(\d{5})</td>\s*<td>(\d{5})</td>\s*<td>MC</td>\s*<td>([A-Z0-9]{8,11})</td>`).FindAllStringSubmatch(string(buf), -1)

	blzToBic := make(map[string]swift.BIC, len(matches))
	for _, m := range matches {
		bic := m[3]
		blz := leftPadZeros(m[1], 5) + leftPadZeros(m[2], 5)

		if _, ok := blzToBic[blz]; ok {
			return nil, fmt.Errorf("MC: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
		}

		// manual fix of bic typos
		switch bic {
		case "EFGBMCMCXX":
			bic = "EFGBMCMCXXX" // trailing X missing
		}
		blzToBic[blz], err = swift.NewBIC(bic)
		if err != nil {
			return nil, fmt.Errorf("MC: unsupported BIC: %s: %w", bic, err)
		}
	}

	return blzToBic, nil
}
