// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"

	"code.pfad.fr/swift"
)

func parseCHjson(jsonFile *os.File) (map[string]swift.BIC, error) {
	var bankMaster struct {
		TotalSize int       `json:"totalSize"`
		ValidOn   string    `json:"validOn"`
		ReadTime  time.Time `json:"readTime"`
		Entries   []struct {
			EntryType               string `json:"entryType"`
			Iid                     int    `json:"iid"`
			ValidOn                 string `json:"validOn"`
			SicIid                  string `json:"sicIid"`
			HeadQuarters            int    `json:"headQuarters"`
			IidType                 string `json:"iidType"`
			BankOrInstitutionName   string `json:"bankOrInstitutionName"`
			StreetName              string `json:"streetName"`
			PostCode                string `json:"postCode"`
			TownName                string `json:"townName"`
			Country                 string `json:"country"`
			Bic                     string `json:"bic"`
			SicParticipation        bool   `json:"sicParticipation"`
			RtgsCustomerPaymentsChf bool   `json:"rtgsCustomerPaymentsChf"`
			IPCustomerPaymentsChf   bool   `json:"ipCustomerPaymentsChf"`
			EuroSicParticipation    bool   `json:"euroSicParticipation"`
			LsvBddChfParticipation  bool   `json:"lsvBddChfParticipation"`
			LsvBddEurParticipation  bool   `json:"lsvBddEurParticipation"`
		} `json:"entries"`
	}

	err := json.NewDecoder(jsonFile).Decode(&bankMaster)
	if err != nil {
		return nil, err
	}

	blzToBic := make(map[string]swift.BIC)
	for _, entry := range bankMaster.Entries {
		blz := leftPadZeros(strconv.Itoa(entry.Iid), 5)
		bic := entry.Bic

		if bic == "" {
			continue
		}
		if _, ok := blzToBic[blz]; ok {
			return nil, fmt.Errorf("CH: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
		}
		blzToBic[blz], err = swift.NewBIC(bic)
		if err != nil {
			return nil, fmt.Errorf("CH: unsupported BIC: %s: %w", bic, err)
		}
	}

	return blzToBic, nil
}
