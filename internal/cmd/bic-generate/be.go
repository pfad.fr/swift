// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"archive/zip"
	"errors"
	"fmt"
	"os"
	"strconv"

	"code.pfad.fr/swift"
	"github.com/thedatashed/xlsxreader"
)

func parseBExlsx(zipFile *os.File) (map[string]swift.BIC, error) {
	fi, err := zipFile.Stat()
	if err != nil {
		return nil, err
	}

	reader, err := zip.NewReader(zipFile, fi.Size())
	if err != nil {
		return nil, err
	}

	xlsxFile, err := xlsxreader.NewReaderZip(reader)
	if err != nil {
		return nil, err
	}

	knownInvalidBIC := map[string]bool{
		"nav": false, "NAV": false, // not available?
		"VRIJ": false, "-": false, // LIBRE
		"NAP": false, // Indisponible
		"NYA": false, // CNH Industrial Capital EUROPE
	}

	blzToBic := make(map[string]swift.BIC)
	for _, sheet := range xlsxFile.Sheets {
		for row := range xlsxFile.ReadRows(sheet) {
			if row.Error != nil {
				return nil, row.Error
			}
			if len(row.Cells) < 3 {
				continue
			}
			from := row.Cells[0].Value
			to := row.Cells[1].Value
			bic := row.Cells[2].Value

			if from == "From" {
				continue // header line
			}
			if _, ok := knownInvalidBIC[bic]; ok {
				knownInvalidBIC[bic] = true
				continue
			}
			bicStruct, err := swift.NewBIC(bic)
			if err != nil {
				return nil, fmt.Errorf("BE %q: %w", bic, err)
			}

			blzRange, err := numberRange(from, to)
			if err != nil {
				return nil, fmt.Errorf("BE %q: %w", bic, err)
			}
			for _, blz := range blzRange {
				if _, ok := blzToBic[blz]; ok {
					return nil, fmt.Errorf("BE: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
				}
				blzToBic[blz] = bicStruct
			}
		}
	}
	for bic, used := range knownInvalidBIC {
		if !used {
			err = errors.Join(err, errors.New(strconv.Quote(bic)))
		}
	}
	if err != nil {
		return blzToBic, fmt.Errorf("BE: unused exceptions:\n%w", err)
	}
	return blzToBic, nil
}

func numberRange(from, to string) ([]string, error) {
	i, err := strconv.Atoi(from)
	if err != nil {
		return nil, err
	}
	end, err := strconv.Atoi(to)
	if err != nil {
		return nil, err
	}
	r := []string{}
	for i <= end {
		r = append(r, leftPadZeros(strconv.Itoa(i), len(to)))
		i++
	}
	return r, nil
}
