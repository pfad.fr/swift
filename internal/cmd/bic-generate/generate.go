// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

// bic-generate generates the exposed bic package from bank BIC listings.
package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"code.pfad.fr/swift"
)

var rootFolder = "."

func main() {
	if len(os.Args) > 1 {
		rootFolder = os.Args[1]
	}

	defer os.RemoveAll(rootFolder + "/data") // comment to keep downloaded data
	if err := generate(); err != nil {
		log.Fatal(err)
	}
}

type generator struct {
	CountryCode     string
	CountryName     string
	Copyright       string
	URL             string
	DynamicDownload func(string) (string, error)
	Filename        string
	FileToMap       func(*os.File) (map[string]swift.BIC, error)
}

func staticURL(s string) func() (string, error) {
	return func() (string, error) {
		return s, nil
	}
}

var generators = []generator{
	{
		// "SEPA-Zahlungsverkehrs-Verzeichnis"
		// https://www.oenb.at/Statistik/Klassifikationen/SEPA-Zahlungsverkehrs-Verzeichnis.html
		CountryCode: "AT",
		CountryName: "Austria",
		Copyright:   "Oesterreichische Nationalbank",
		URL:         "https://www.oenb.at/docroot/downloads_observ/sepa-zv-vz_gesamt.csv",
		Filename:    "AT.csv",
		FileToMap:   parseATcsv,
	},
	{
		// "Code d'identification des Banques" > "Grouped List of Current Codes (xlsx)"
		// https://www.nbb.be/fr/paiements-et-titres/standards-de-paiements/codes-didentification-des-banques
		CountryCode: "BE",
		CountryName: "Belgium",
		Copyright:   "Banque nationale de Belgique",
		URL:         "https://www.nbb.be/doc/be/be/protocol/current_codes.xlsx",
		Filename:    "BE.xlsx",
		FileToMap:   parseBExlsx,
	},
	{
		// "Download Schweizer Bankenstamm" > "Bankenstamm REST API V 3.0 (JSON)"
		// https://www.six-group.com/de/products-services/banking-services/interbank-clearing/online-services/download-bank-master.html
		CountryCode: "CH",
		CountryName: "Switzerland",
		Copyright:   "SIX Group Services AG",
		URL:         "https://api.six-group.com/api/epcd/bankmaster/v3/bankmaster.json",
		Filename:    "CH.json",
		FileToMap:   parseCHjson,
	},
	{
		// "Download - Bankleitzahlendateien" > "Bankleitzahlendateien gepackt (Text)"
		// https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592
		CountryCode:     "DE",
		CountryName:     "Germany",
		Copyright:       "Deutsche Bundesbank",
		DynamicDownload: downloadDE,
		Filename:        "DE.zip",
		FileToMap:       parseDEzip,
	},
	{
		// see ecbLatestLink
		CountryCode: "ES",
		CountryName: "Spain",
		Copyright:   "European Central Bank",
		DynamicDownload: func(string) (string, error) {
			return downloadEUOnce()
		},
		Filename:  "EU.csv.gz",
		FileToMap: parseEUcsv("ES", 4),
	},
	{
		// see ecbLatestLink
		CountryCode: "FR",
		CountryName: "France",
		Copyright:   "European Central Bank",
		DynamicDownload: func(string) (string, error) {
			return downloadEUOnce()
		},
		Filename:  "EU.csv.gz",
		FileToMap: parseEUcsv("FR", 5),
	},
	{
		// "Mitglieder :: Liechtensteinischer Bankenverband"
		CountryCode: "LI",
		CountryName: "Liechtenstein",
		Copyright:   "Liechtensteinischer Bankenverband e.V.",
		URL:         "https://www.bankenverband.li/verband/organisation/mitglieder",
		Filename:    "LI.html",
		FileToMap:   parseLIhtml,
	},
	{
		// retrieved on 2023-11-14 from "Luxembourg register of IBAN/BIC codes" > "IBAN/BIC code table"
		// https://www.abbl.lu/en/professionals/page/iban-and-bic-codes
		CountryCode:     "LU",
		CountryName:     "Luxembourg",
		Copyright:       "Association des Banques et Banquiers, Luxembourg (ABBL)",
		DynamicDownload: downloadLU,
		Filename:        "LU.xlsx",
		FileToMap:       parseLUxlsx,
	},
	{
		// "BIC-IBAN"
		CountryCode: "MC",
		CountryName: "Monaco",
		Copyright:   "Association Monégasque des Activités Financières (AMAF)",
		URL:         "https://www.amaf.mc/fr/bic-iban",
		Filename:    "MC.html",
		FileToMap:   parseMChtml,
	},
	{
		// "Business Identifier Code" > "Klik hier voor het overzicht als Excel-bestand"
		// https://www.betaalvereniging.nl/betalingsverkeer/giraal-betalingsverkeer/bic-sepa-transacties/
		// https://www.betaalvereniging.nl/en/focus/giro-based-and-online-payments/business-identifier-code-bic-for-sepa-transactions/
		CountryCode: "NL",
		CountryName: "Netherlands",
		Copyright:   "Dutch Payments Association",
		URL:         "https://www.betaalvereniging.nl/wp-content/uploads/BIC-lijst-NL.xlsx",
		Filename:    "NL.xlsx",
		FileToMap:   parseNLxlsx,
	},
}

func (g generator) Generate(packageFolder string) (int, error) {
	tmpPath := filepath.Join(packageFolder, "data", g.Filename)
	f, err := os.Open(tmpPath)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	mapped, err := g.FileToMap(f)
	if err != nil {
		return 0, err
	}

	return g.writeMapInGo(packageFolder, mapped)
}

func generate() error {
	var wg sync.WaitGroup
	// treat http redirect as error (to prevent silent failures)
	http.DefaultClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	errs := make([]error, len(generators))
	lengths := make([]int, len(generators))
	for i, g := range generators {
		i := i
		g := g
		wg.Add(1)
		go func() {
			defer wg.Done()
			start := time.Now()
			if g.DynamicDownload != nil {
				url, err := g.DynamicDownload(rootFolder + "/data/" + g.Filename)
				if err != nil {
					errs[i] = fmt.Errorf("%s could not download: %w", g.CountryCode, err)
					return
				}
				g.URL = url
			} else {
				if err := downloadFile(g.URL, rootFolder+"/data/"+g.Filename); err != nil {
					errs[i] = fmt.Errorf("%s could not download: %w", g.CountryCode, err)
					return
				}
			}
			var err error
			if lengths[i], err = g.Generate(rootFolder); err != nil {
				errs[i] = fmt.Errorf(" %s could not generate: %w", g.CountryCode, err)
				return
			}
			log.Println(g.CountryCode, "generated in", time.Since(start).Truncate(time.Millisecond))
		}()
	}
	wg.Wait()

	if err := writeBicDotGo(lengths); err != nil {
		return fmt.Errorf("could not write bic.go: %w", err)
	}

	return errors.Join(errs...)
}

func writeBicDotGo(lengths []int) error {
	glueCode := `// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+
// Code generated by code.pfad.fr/swift/internal/cmd/bic-generate DO NOT EDIT.

// package bic computes the BIC from an IBAN (for supported countries).
package bic

import "code.pfad.fr/swift"

// FromIBAN returns the BIC from an IBAN.
// If the BIC cannot be computed, it returns an empty BIC. Supported country codes:
//
`
	for _, g := range generators {
		glueCode += "//   - " + g.CountryCode + " (" + g.CountryName + ")\n"
	}
	glueCode += `func FromIBAN(iban swift.IBAN) swift.BIC {
	switch iban.CountryCode {`
	for i, g := range generators {
		end := strconv.Itoa(lengths[i])
		glueCode += `
	case "` + g.CountryCode + `":
		if len(iban.BBAN) >= ` + end + ` {
			return ` + strings.ToLower(g.CountryCode) + `BLZtoBIC[iban.BBAN[:` + end + `]]
		}`
	}
	glueCode += `
	}

	return swift.BIC{}
}
`

	return os.WriteFile(rootFolder+"/bic.go", []byte(glueCode), 0o666)
}

func downloadFile(url, name string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}

	if err := os.MkdirAll(path.Dir(name), os.ModePerm); err != nil {
		return err
	}
	dst, err := os.Create(name)
	if err != nil {
		return err
	}
	defer dst.Close()
	_, err = io.Copy(dst, resp.Body)
	return err
}

func (g generator) writeMapInGo(packageFolder string, value map[string]swift.BIC) (int, error) {
	// check that all keys have the same length
	expectedLen := 0
	for key := range value {
		if expectedLen == 0 {
			expectedLen = len(key)
		} else if expectedLen != len(key) {
			return 0, fmt.Errorf("expected %d chars, got %d for %q", expectedLen, len(key), key)
		}
	}

	cc := strings.ToLower(g.CountryCode)
	filename := filepath.Join(packageFolder, cc+".go")

	mapStr := fmt.Sprintf("%#v", value)
	mapStr = strings.ReplaceAll(mapStr, `"}, "`, `"},
	"`)
	mapStr = strings.ReplaceAll(mapStr, `":swift.BIC{Institution:"`, `": {"`)
	mapStr = strings.ReplaceAll(mapStr, `", CountryCode:"`, `", "`)
	mapStr = strings.ReplaceAll(mapStr, `", Location:"`, `", "`)
	mapStr = strings.ReplaceAll(mapStr, `", Branch:"`, `", "`)
	mapStr = strings.Replace(mapStr, `swift.BIC{"`, `swift.BIC{
	"`, 1)
	mapStr = strings.Replace(mapStr, `"}}`, `"},
}
`, 1)
	content := `// SPDX-License-Identifier: EUPL-1.2+
// SPDX-FileCopyrightText: ` + g.Copyright + `
// Source: ` + g.URL + `
// Code generated by code.pfad.fr/swift/internal/cmd/bic-generate DO NOT EDIT.

package bic

import "code.pfad.fr/swift"

var ` + cc + `BLZtoBIC = ` + mapStr

	return expectedLen, os.WriteFile(filename, []byte(content), 0o666)
}

func leftPadZeros(s string, l int) string {
	missing := l - len(s)
	if missing <= 0 {
		return s
	}
	return strings.Repeat("0", missing) + s
}

// extract the .csv.gz link from  "Credit Institutions (Monetary Financial Institutions and Systemic Investment Firms): minimum reserve requirement features (monthly data). Download area" > "Full Database"
var downloadEUOnce = sync.OnceValues(func() (string, error) {
	uri, err := findRegex(regexp.MustCompile(`<a href="(\/[^"]+\.csv\.gz)">`),
		"https://www.ecb.europa.eu/stats/financial_corporations/list_of_financial_institutions/html/monthly_list-MID.en.html")
	if err != nil {
		return "", fmt.Errorf("ecb.europa.eu: link to .csv.gz: %w", err)
	}
	url := "https://www.ecb.europa.eu" + uri

	if err := downloadFile(url, rootFolder+"/data/EU.csv.gz"); err != nil {
		return url, fmt.Errorf("%s could not download: %w", "EU", err)
	}
	return url, nil
})

func downloadDE(dst string) (string, error) {
	uri, err := findRegex(regexp.MustCompile(`<a href="(\/[^"]+blz-aktuell-csv-zip-data\.zip)" [^>]+>`),
		"https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592")
	if err != nil {
		return "", fmt.Errorf("www.bundesbank.de: link to blz-aktuell-csv-zip-data.zip: %w", err)
	}
	url := "https://www.bundesbank.de" + uri

	if err := downloadFile(url, dst); err != nil {
		return url, err
	}
	return url, nil
}

func downloadLU(dst string) (string, error) {
	uri, err := findRegex(regexp.MustCompile(`href="(\/[^"]+Luxembourg Register of IBAN-BIC[^"]+\.xlsx)"`),
		"https://www.abbl.lu/en/professionals/page/iban-and-bic-codes")
	if err != nil {
		return "", fmt.Errorf("www.abbl.lu: link to Register of IBAN-BIC: %w", err)
	}
	url := "https://www.abbl.lu" + uri

	if err := downloadFile(url, dst); err != nil {
		return url, err
	}
	return url, nil
}

func findRegex(re *regexp.Regexp, url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected http status code: %d", resp.StatusCode)
	}

	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		s := scanner.Text()
		found := re.FindStringSubmatch(s)
		if len(found) > 0 {
			return found[1], nil
		}
	}
	if err := scanner.Err(); err != nil {
		return "", err
	}

	return "", errors.New("regexp not matched")
}
