// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"compress/gzip"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"code.pfad.fr/swift"
	"golang.org/x/text/encoding/unicode"
	"golang.org/x/text/transform"
)

// Thanks to https://github.com/sigalor/iban-to-bic/blob/main/src/fr-es.js (MIT licensed)
// for the trick of extracting the bank code from the "riad code" in the european provided file.
func parseEUcsv(countryCode string, leftPad int) func(csvFile *os.File) (map[string]swift.BIC, error) {
	return func(csvFile *os.File) (map[string]swift.BIC, error) {
		blzToBic := make(map[string]swift.BIC)
		zr, err := gzip.NewReader(csvFile)
		if err != nil {
			return nil, err
		}
		defer zr.Close()

		dec := unicode.UTF16(unicode.LittleEndian, unicode.UseBOM).NewDecoder()
		reader := csv.NewReader(transform.NewReader(zr, dec))
		reader.Comma = '\t'
		reader.LazyQuotes = true
		for {
			record, err := reader.Read()
			if err != nil {
				if err == io.EOF {
					break
				}
				return nil, err
			}

			if record[2] != countryCode {
				continue
			}

			blz := strings.TrimSpace(record[0][2:])
			bic := strings.TrimSpace(record[1])
			if blz == "" || bic == "" || len(blz) > leftPad {
				continue
			}
			blz = leftPadZeros(blz, leftPad)

			if _, ok := blzToBic[blz]; ok {
				return nil, fmt.Errorf("%s: duplicate BLZ %s: %s/%s", countryCode, blz, bic, blzToBic[blz])
			}
			blzToBic[blz], err = swift.NewBIC(bic)
			if err != nil {
				return nil, fmt.Errorf("%s: unsupported BIC: %s: %w", countryCode, bic, err)
			}
		}

		return blzToBic, nil
	}
}
