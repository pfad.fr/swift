// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"archive/zip"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"slices"
	"strings"

	"code.pfad.fr/swift"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func parseDEzip(zipFile *os.File) (map[string]swift.BIC, error) {
	fi, err := zipFile.Stat()
	if err != nil {
		return nil, err
	}
	reader, err := zip.NewReader(zipFile, fi.Size())
	if err != nil {
		return nil, err
	}

	entry := func(r *zip.Reader) *zip.File {
		for _, f := range r.File {
			if strings.HasPrefix(f.Name, "BLZ") && strings.HasSuffix(strings.ToLower(f.Name), ".csv") {
				return f
			}
		}
		return nil
	}(reader)

	if entry == nil {
		var names []string
		for _, f := range reader.File {
			names = append(names, f.Name)
		}
		return nil, fmt.Errorf("could not find BLZ***.csv inside zip: %v", names)
	}
	f, err := entry.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()

	blzToBic := make(map[string]swift.BIC)
	scanner := csv.NewReader(transform.NewReader(f, charmap.Windows1252.NewDecoder()))
	scanner.Comma = ';'

	header, err := scanner.Read()
	if err != nil {
		return nil, err
	}
	index := struct {
		blz, merkmal, bic int
	}{
		blz:     slices.Index(header, "Bankleitzahl"),
		merkmal: slices.Index(header, "Merkmal"),
		bic:     slices.Index(header, "BIC"),
	}
	if index.blz < 0 || index.merkmal < 0 || index.bic < 0 {
		return nil, fmt.Errorf("unexpected header %v", header)
	}

	for {
		row, err := scanner.Read()
		if err != nil {
			if err == io.EOF {
				return blzToBic, nil
			}
			return blzToBic, err
		}
		blz := row[index.blz]
		bic := row[index.bic]
		if bic == "" {
			continue
		}

		// '1' at colum "merkmal", means "payment service provider" (= preferred bic)
		if _, ok := blzToBic[blz]; ok && row[index.merkmal] != "1" {
			continue
		}
		blzToBic[blz], err = swift.NewBIC(bic)
		if err != nil {
			return nil, fmt.Errorf("invalid BIC: %s: %w", bic, err)
		}
	}
}
