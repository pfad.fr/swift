// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"archive/zip"
	"fmt"
	"os"
	"strings"

	"code.pfad.fr/swift"
	"github.com/thedatashed/xlsxreader"
)

func parseLUxlsx(zipFile *os.File) (map[string]swift.BIC, error) {
	fi, err := zipFile.Stat()
	if err != nil {
		return nil, err
	}

	reader, err := zip.NewReader(zipFile, fi.Size())
	if err != nil {
		return nil, err
	}

	xlsxFile, err := xlsxreader.NewReaderZip(reader)
	if err != nil {
		return nil, err
	}

	blzToBic := make(map[string]swift.BIC)
	for _, sheet := range xlsxFile.Sheets {
		for row := range xlsxFile.ReadRows(sheet) {
			if row.Error != nil {
				return nil, row.Error
			}
			if len(row.Cells) < 3 {
				continue
			}
			bic := row.Cells[2].Value
			blz := row.Cells[1].Value

			if strings.TrimSpace(bic) == "BIC Code" {
				continue // header line
			}
			bicStruct, err := swift.NewBIC(bic)
			if err != nil {
				return nil, err
			}

			if _, ok := blzToBic[blz]; ok {
				return nil, fmt.Errorf("LU: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
			}
			blzToBic[blz] = bicStruct
		}
	}
	return blzToBic, nil
}
