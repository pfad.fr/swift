// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"errors"
	"io/fs"
	"testing"
)

func TestGenerate(t *testing.T) {
	generateIfLocalExists(t, "AT", false)
	generateIfLocalExists(t, "BE", false)
	generateIfLocalExists(t, "CH", false)
	generateIfLocalExists(t, "DE", false)
	generateIfLocalExists(t, "ES", false)
	generateIfLocalExists(t, "FR", false)
	generateIfLocalExists(t, "LI", false)
	generateIfLocalExists(t, "LU", false)
	generateIfLocalExists(t, "MC", false)
	generateIfLocalExists(t, "NL", false)
}

func generateIfLocalExists(t *testing.T, countryCode string, download bool) {
	t.Run(countryCode, func(t *testing.T) {
		const root = "../../../bic"
		for _, g := range generators {
			if g.CountryCode != countryCode {
				continue
			}
			if download {
				if err := downloadFile(g.URL, root+"/data/"+g.Filename); err != nil {
					t.Fatal(err)
				}
			}
			if _, err := g.Generate(root); err != nil {
				var perr *fs.PathError
				if !download && errors.Is(err, fs.ErrNotExist) && errors.As(err, &perr) && perr.Path == root+"/data/"+g.Filename {
					t.Skip(err) // local data file does not exist: skip test
					return
				}
				t.Fatal(err)
			}
			return
		}
		t.Fatalf("unknown countryCode: %s", countryCode)
	})
}
