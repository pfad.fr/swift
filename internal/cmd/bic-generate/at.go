// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"

	"code.pfad.fr/swift"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

func parseATcsv(csvFile *os.File) (map[string]swift.BIC, error) {
	blzToBic := make(map[string]swift.BIC)
	reader := csv.NewReader(transform.NewReader(csvFile, charmap.Windows1252.NewDecoder()))
	reader.Comma = ';'
	headerRead := false
	for {
		record, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if len(record) < 20 {
			reader.FieldsPerRecord = 0
			continue
		}
		if !headerRead {
			headerRead = true
			continue
		}

		blz := strings.TrimSpace(record[2])
		bic := strings.TrimSpace(record[18])
		if blz == "" || bic == "" {
			continue
		}
		blz = leftPadZeros(blz, 5)

		if _, ok := blzToBic[blz]; ok {
			return nil, fmt.Errorf("AT: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
		}
		blzToBic[blz], err = swift.NewBIC(bic)
		if err != nil {
			return nil, fmt.Errorf("AT: unsupported BIC: %s: %w", bic, err)
		}
	}

	return blzToBic, nil
}
