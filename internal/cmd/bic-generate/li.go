// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"fmt"
	"io"
	"os"
	"regexp"

	"code.pfad.fr/swift"
)

func parseLIhtml(htmlFile *os.File) (map[string]swift.BIC, error) {
	buf, err := io.ReadAll(htmlFile)
	if err != nil {
		return nil, err
	}

	matches := regexp.MustCompile(`Swift-Code(?:\s|&nbsp;)*([A-Z0-9]{8,11})(?:\s|&nbsp;)*<br />\s*BC-Nr. I BC-No.(?:\s|&nbsp;)*([0-9]{1,5})`).FindAllStringSubmatch(string(buf), -1)

	blzToBic := make(map[string]swift.BIC, len(matches))
	for _, m := range matches {
		bic := m[1]
		blz := leftPadZeros(m[2], 5)

		if _, ok := blzToBic[blz]; ok {
			return nil, fmt.Errorf("LI: duplicate BLZ %s: %s/%s", blz, bic, blzToBic[blz])
		}

		blzToBic[blz], err = swift.NewBIC(bic)
		if err != nil {
			return nil, fmt.Errorf("LI: unsupported BIC: %s: %w", bic, err)
		}
	}

	return blzToBic, nil
}
