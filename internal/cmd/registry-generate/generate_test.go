// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package main

import (
	"errors"
	"io/fs"
	"os"
	"testing"
)

func TestRead(t *testing.T) {
	downloadIfMissing := false

	f, err := os.Open("./iban-registry.csv")
	if err != nil {
		if !errors.Is(err, fs.ErrNotExist) {
			t.Fatal(err)
		} else if !downloadIfMissing {
			t.Skip(registryURL + " not downloaded as iban-registry.csv")
		}

		// download before proceeding
		body, err := requestTXT()
		if err != nil {
			t.Fatal(err)
		}
		defer body.Close()

		cf, err := os.Create("./iban-registry.csv")
		if err != nil {
			t.Fatal(err)
		}
		if _, err = cf.ReadFrom(body); err != nil {
			t.Fatal(err)
		}
		if err = cf.Close(); err != nil {
			t.Fatal(err)
		}

		f, err = os.Open("./iban-registry.csv")
		if err != nil {
			t.Fatal(err)
		}
	}
	t.Cleanup(func() { f.Close() })

	if err := generate("../../..", f); err != nil {
		t.Fatal(err)
	}
}
