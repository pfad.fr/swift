// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

// registry-generate generates the bban and country checkers from the SWIFT IBAN registry.
//
// https://www.swift.com/resource/iban-registry-txt
package main

import (
	"cmp"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"slices"
	"strings"

	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
)

const registryURL = "https://www.swift.com/resource/iban-registry-txt"

// https://en.wikipedia.org/wiki/International_Bank_Account_Number#National_check_digits
var countryCheckers = map[string]string{
	// "AL": Albania[16] 	Weighted 	9, 7, 3, 1, 9, 7, 3, 1 	10 	10 − r, 0 → 0 	Applies only to the bank code + branch code fields.
	"BA": "mod97Checker(98, 14)",
	"BE": "beCheckDigits",
	// "CZ": Czech Republic[17] 	Weighted 	6, 3, 7, 9, 10, 5, 8, 4, 2, 1 	11 	11 − r, 0 → 0 	Calculated separately for the account number (ten digits) and branch number (six digits, using the last six weights). The last digit of each value is its check digit.
	// "EE": Estonia[17][19][20] 	Weighted 	7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7 	10 	10 − r, 0 → 0 	Applies only to the branch code + account number fields (ignoring the bank code).
	// "ES": Spain[17] 	Weighted 	1, 2, 4, 8, 5, 10, 9, 7, 3, 6 	11 	11 − r, 0 → 0, 1 → 1 	There are two separate check digits—one for the bank code + branch code, and one for the account number, each calculated separately. The account number is ten characters long and uses all of the weights, whereas the bank code + branch code are eight characters long and thus use only the last eight weights in the calculation (or equivalently, pad with two zeros on the left and use the ten weights).
	// "FI": Finland[17] 	Luhn 	2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 	10 	10 − r, 0 → 0 	Uses the Luhn Algorithm, where the sum is taken of the individual digits of the multiplication products rather than the products themselves.
	"FR": "frCheckDigits",
	"HR": "hrCheckDigits",
	// "HU": Hungary[17] 	Weighted 	9, 7, 3, 1, 9, 7, 3, 1, 9, 7, 3, 1, 9, 7, 3, 1 	10 	10 − r, 0 → 0 	There are two separate check digits—one for the bank code + branch code, and one for the account number, each calculated separately.
	// "IS": Iceland[17] 	Weighted 	3, 2, 7, 6, 5, 4, 3, 2 	11 	11 − r, 0 → 0 	Applies only to the first eight digits of the national identification number (kennitala), with the check digit stored at the 9th.
	// "IT": Italy[17] 	Conversion + Sum 		26 	r 	Characters are converted to digits using two different conversion tables, one for odd positions and one for even positions (the first character is considered odd).
	//       Odd-positioned digits 0–9 are converted to their respective values in the sequence 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, and characters in the range A–Z are converted to 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 respectively. Even-positioned characters are converted using the natural zero-based value, i.e. digits 0–9 converted to the respective numbers 0–9, and letters A–Z to the range 0–25. After conversion the numbers are summed (without weights), and the result taken modulo 26. This is then converted back into a single letter in the range A–Z (in natural order) which is used as the check digit (or rather, check character).
	"MC": "frCheckDigits",
	"ME": "mod97Checker(98, 16)",
	"MK": "mod97Checker(98, 13)",
	"MR": "mod97Checker(97, 21)",
	// "NO": Norway[17] 	Weighted 	5, 4, 3, 2, 7, 6, 5, 4, 3, 2 	11 	11 − r, 0 → 0, 1 → invalid 	If the first two digits of the account number (not the bank code) are both zeros, then the calculation applies only to the remaining four digits of the account number, otherwise it applies to the entire BBAN (bank code + account number).
	// "PL": Poland[17][22] 	Weighted 	3, 9, 7, 1, 3, 9, 7 	10 	10 − r, 0 → 0 	Applies only to the bank code + branch code (without the account number).
	"PT": "mod97Checker(98, 19)",
	"RS": "mod97Checker(98, 16)",
	"SI": "mod97Checker(98, 13)",
	// "SK": Slovakia[17] 	Weighted 	6, 3, 7, 9, 10, 5, 8, 4, 2, 1 	11 	11 − r, 0 → 0 	Calculated separately for the account number (ten digits) and branch number (six digits, using the last six weights). Same as Czech Republic.
	// "SM": San Marino[17] 	Conversion + Sum 		26 	r 	Uses the same algorithm as Italy.
	"TL": "mod97Checker(98, 17)",
	"TN": "mod97Checker(97, 18)",
}

var rootFolder = "."

func main() {
	if len(os.Args) > 1 {
		rootFolder = os.Args[1]
	}
	body, err := requestTXT()
	if err != nil {
		log.Fatal(err)
	}
	defer body.Close()

	if err := generate(rootFolder, body); err != nil {
		log.Fatal(err)
	}
}

func requestTXT() (io.ReadCloser, error) {
	req, err := http.NewRequest("GET", registryURL, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:130.0) Gecko/20100101 Firefox/130.0")
	req.Header.Set("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8")
	req.Header.Set("Accept-Language", "en-US,en;q=0.5")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}

func generate(packageFolder string, csvFile io.Reader) error {
	countries, err := readCSV(csvFile)
	if err != nil {
		return err
	}
	{
		out, err := os.Create(filepath.Join(packageFolder, "bban_registry.go"))
		if err != nil {
			return err
		}
		if err := writeRegistryLines(out, countries); err != nil {
			return err
		}
		if err := out.Close(); err != nil {
			return err
		}
	}
	{
		out, err := os.Create(filepath.Join(packageFolder, "bban_registry_test.go"))
		if err != nil {
			return err
		}
		if err := writeRegistryTest(out, countries); err != nil {
			return err
		}
		if err := out.Close(); err != nil {
			return err
		}
	}
	return nil
}

func writeRegistryTest(w io.StringWriter, countries []*country) error {
	_, err := w.WriteString(`// SPDX-License-Identifier: EUPL-1.2+
// SPDX-FileCopyrightText: S.W.I.F.T. SC
// Source: ` + registryURL + `
// Code generated by code.pfad.fr/swift/internal/cmd/registry-generate DO NOT EDIT.

package swift_test

import "testing"

func TestBBANRegistry(t *testing.T) {
`)
	if err != nil {
		return err
	}
	for _, country := range countries {
		_, err = w.WriteString(`	validIBAN(t, "` + country.Example + `")
`)
		if err != nil {
			return err
		}
	}
	_, err = w.WriteString(`}
`)
	return err
}

func writeRegistryLines(w io.Writer, countries []*country) error {
	_, err := w.Write([]byte(`// SPDX-License-Identifier: EUPL-1.2+
// SPDX-FileCopyrightText: S.W.I.F.T. SC
// Source: ` + registryURL + `
// Code generated by code.pfad.fr/swift/internal/cmd/registry-generate DO NOT EDIT.

package swift

import "code.pfad.fr/swift/internal/format"

var bbanRegistryByCountryCode = map[CountryCode]bbanChecker{
`))
	if err != nil {
		return err
	}
	for _, country := range countries {
		countryChecker := cmp.Or(countryCheckers[country.CountryCode], "nil")
		delete(countryCheckers, country.CountryCode)
		columns := make([]string, 0)
		bbanStructRegex.ReplaceAllStringFunc(country.BBANStruct, func(m string) string {
			columns = append(columns, patternToGo(m))
			return m
		})
		if strings.Count(country.BBANStruct, "!") != len(columns) {
			return fmt.Errorf("incomplete conversion %v to %v", country.BBANStruct, columns)
		}
		fmt.Fprintf(w, `	%q: {%s, columns{%s}, %v},
`,
			country.CountryCode,
			countryChecker,
			strings.Join(columns, ", "),
			country.IsSEPA,
		)
	}
	if len(countryCheckers) > 0 {
		return fmt.Errorf("unused countryCheckers: %v", countryCheckers)
	}

	_, err = w.Write([]byte(`}
`))
	return err
}

var bbanStructRegex = regexp.MustCompile(`\d+![anc]`)

func patternToGo(pattern string) string {
	var funcName string
	switch pattern[len(pattern)-1] {
	case 'n':
		funcName = "Numeric"
	case 'a':
		funcName = "Alpha"
	case 'c':
		funcName = "Mixed"
	default:
		panic(pattern)
	}
	count := pattern[:len(pattern)-2]
	return "format." + funcName + "(" + count + ")"
}

type country struct {
	CountryCode string
	IsSEPA      bool
	BBANStruct  string
	Example     string
}

// manual workarounds
var correctedIBANStructures = map[string]string{
	"NO4!n6!n1!n": "4!n6!n1!n",
}

func readCSV(csvFile io.Reader) (countries []*country, err error) {
	reader := csv.NewReader(transform.NewReader(csvFile, charmap.Windows1252.NewDecoder()))
	reader.Comma = '	'

	bbanStructRegex := regexp.MustCompile(`^(\d+![anc])+$`)

	for {
		record, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		switch record[0] {
		case "IBAN prefix country code (ISO 3166)":
			countries = make([]*country, 0, len(record)-1)
			for _, v := range record[1:] {
				countries = append(countries, &country{
					CountryCode: v,
				})
			}
		case "SEPA country":
			for i, v := range record[1:] {
				switch v {
				case "Yes":
					countries[i].IsSEPA = true

					if countries[i].CountryCode == "IQ" {
						// when deleting this line, delete the block in the case "Nov-16" as well
						return countries, errors.New("IBAN registry useless workaround: IQ SEPA")
					}
				case "Nov-16":
					// manual workaround for invalid info
					if countries[i].CountryCode == "IQ" {
						log.Println("IBAN registry workaround used: IQ SEPA: Nov-16")
					}

				case "No":
				default:
					return countries, fmt.Errorf("unexpected SEPA country: %s %q", countries[i].CountryCode, v)
				}
			}
		case "BBAN structure":
			for i, v := range record[1:] {
				if corrected, ok := correctedIBANStructures[v]; ok {
					delete(correctedIBANStructures, v)
					v = corrected
					log.Printf("IBAN registry workaround used: %s structure\n", v)
				}
				if !bbanStructRegex.MatchString(v) {
					return countries, fmt.Errorf("unexpected IBAN struct: %s", v)
				}

				countries[i].BBANStruct = v
			}
		case "IBAN structure":
			for i, v := range record[1:] {
				expected := countries[i].CountryCode + "2!n" + countries[i].BBANStruct
				if v != expected {
					return countries, fmt.Errorf("expected IBAN struct %s, got %s", expected, v)
				}
			}
		case "IBAN electronic format example":
			for i, v := range record[1:] {
				countries[i].Example = v
			}
		}
	}

	slices.SortFunc(countries, func(a *country, b *country) int {
		return cmp.Compare(a.CountryCode, b.CountryCode)
	})

	if len(correctedIBANStructures) > 0 {
		return countries, fmt.Errorf("IBAN registry unused workarounds: %v", correctedIBANStructures)
	}
	return countries, nil
}
