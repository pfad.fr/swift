// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0
module code.pfad.fr/swift/internal/cmd

go 1.21

require (
	code.pfad.fr/swift v0.8.2
	github.com/thedatashed/xlsxreader v1.2.8
	golang.org/x/text v0.22.0
)

replace code.pfad.fr/swift => ../..
