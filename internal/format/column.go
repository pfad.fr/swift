// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

// package format is used internally to describe the format of an IBAN.
package format

import "strconv"

type ExpectedCharType int

const (
	NoMoreChar = iota + 1
	UppercaseAlphaChar
	AlphaChar
	ExpectedNumericChar
	ExpectedAlphaNumericChar
)

func (e ExpectedCharType) String() string {
	switch e {
	case NoMoreChar:
		return "no more character"
	case UppercaseAlphaChar:
		return "upper case alpha character"
	case AlphaChar:
		return "alpha character"
	case ExpectedNumericChar:
		return "numeric character"
	case ExpectedAlphaNumericChar:
		return "alphanumeric character"
	default:
		return "<unknown-char-type:" + strconv.Itoa(int(e)) + ">"
	}
}

type Column interface {
	Len() int
	Expected() ExpectedCharType
	IndexInvalid(s string) int // returns -1 if the string is valid
}

type Alpha int // (A-Z)
var _ Column = Alpha(0)

func (l Alpha) Len() int                 { return int(l) }
func (Alpha) Expected() ExpectedCharType { return UppercaseAlphaChar }
func (Alpha) IndexInvalid(s string) int {
	for i, c := range s {
		if !isUpperLetter(c) {
			return i
		}
	}
	return -1
}

type AlphaInsensitive int // (a-z, A-Z)
var _ Column = AlphaInsensitive(0)

func (l AlphaInsensitive) Len() int                 { return int(l) }
func (AlphaInsensitive) Expected() ExpectedCharType { return AlphaChar }
func (AlphaInsensitive) IndexInvalid(s string) int {
	for i, c := range s {
		if !(isLowerLetter(c) || isUpperLetter(c)) {
			return i
		}
	}
	return -1
}

type Numeric int // (0-9)
var _ Column = Numeric(0)

func (l Numeric) Len() int                 { return int(l) }
func (Numeric) Expected() ExpectedCharType { return ExpectedNumericChar }
func (Numeric) IndexInvalid(s string) int {
	for i, c := range s {
		if !isNumeric(c) {
			return i
		}
	}
	return -1
}

type Mixed int // (a-z, A-Z, 0-9)
var _ Column = Mixed(0)

func (l Mixed) Len() int                 { return int(l) }
func (Mixed) Expected() ExpectedCharType { return ExpectedAlphaNumericChar }
func (Mixed) IndexInvalid(s string) int {
	for i, c := range s {
		if !(isLowerLetter(c) || isUpperLetter(c) || isNumeric(c)) {
			return i
		}
	}
	return -1
}

func isLowerLetter(c rune) bool {
	return ('a' <= c && c <= 'z')
}

func isUpperLetter(c rune) bool {
	return ('A' <= c && c <= 'Z')
}

func isNumeric(c rune) bool {
	return ('0' <= c && c <= '9')
}
