// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"strings"

	"code.pfad.fr/swift/internal/format"
)

// NewBIC uppercases a BIC and perform some basic checks (format and length).
// If the country code is unknown the BIC will be accepted,
// call [CountryCode.BelongsToIBANRegistry] or [CountryCode.BelongsToSEPA] to perform
// additional validation.
func NewBIC(s string) (BIC, error) {
	s = strings.ToUpper(nonAlphanumeric.ReplaceAllString(s, ""))

	branchLength := 0
	if len(s) > 8 {
		branchLength = 3
	}

	cc := columns{
		format.Alpha(4), // institution
		format.Alpha(2), // country
		format.Mixed(2), // location
		format.Mixed(branchLength),
	}
	parts, err := cc.match(s)
	var bic BIC
	if len(parts) > 0 {
		bic.Institution = parts[0]
	}
	if len(parts) > 1 {
		bic.CountryCode = CountryCode(parts[1])
	}
	if len(parts) > 2 {
		bic.Location = parts[2]
	}
	if len(parts) > 3 {
		bic.Branch = parts[3]
	}
	return bic, err
}

// BIC represents the routing information as specified by ISO 9362 (also known as Business Identifier Codes (BIC), SWIFT ID or SWIFT code, and SWIFT-BIC).
type BIC struct {
	Institution string      // 4 letters
	CountryCode CountryCode // 2 letters (ISO 3166-1 alpha-2 code)
	Location    string      // 2 letters or digits
	Branch      string      // 3 letters or digits (optional)
}

// String returns the BIC.
func (b BIC) String() string {
	return b.Institution + string(b.CountryCode) + b.Location + b.Branch
}
