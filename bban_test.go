// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import "testing"

func TestBBANLength(t *testing.T) {
	for country, bc := range bbanRegistryByCountryCode {
		t.Run(string(country), func(t *testing.T) {
			l := bc.columns.len()
			if l < minBBANLength {
				t.Errorf("minBBANLength is too high %d, got %d", minBBANLength, l)
			}
			if l > maxBBANLength {
				t.Errorf("maxBBANLength is too low %d, got %d", maxBBANLength, l)
			}
		})
	}
}
