// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"errors"
	"testing"

	"code.pfad.fr/check"
)

func TestParseValidBIC(t *testing.T) {
	{
		bic, err := NewBIC("DEUTDEFF")
		check.Equal(t, err, nil)
		check.Equal(t, bic, BIC{
			Institution: "DEUT",
			CountryCode: "DE",
			Location:    "FF",
			Branch:      "",
		})
		check.Equal(t, bic.String(), "DEUTDEFF")
	}
	{
		bic, err := NewBIC("DEUTDEFF500")
		check.Equal(t, err, nil)
		check.Equal(t, bic, BIC{
			Institution: "DEUT",
			CountryCode: "DE",
			Location:    "FF",
			Branch:      "500",
		})
		check.Equal(t, bic.String(), "DEUTDEFF500")
	}
	{
		bic, err := NewBIC("dEUTDeFf500")
		check.Equal(t, err, nil)
		check.Equal(t, bic, BIC{
			Institution: "DEUT",
			CountryCode: "DE",
			Location:    "FF",
			Branch:      "500",
		})
		check.Equal(t, bic.String(), "DEUTDEFF500")
	}
}

func TestParseInvalidBIC(t *testing.T) {
	{
		_, err := NewBIC("DEUTDEF")
		check.Equal(t, err.(TooShortError), TooShortError{
			ActualLen:   7,
			ExpectedLen: 8,
		})
	}
	{
		_, err := NewBIC("DEUTDEFF5")
		check.Equal(t, err.(TooShortError), TooShortError{
			ActualLen:   9,
			ExpectedLen: 11,
		})
	}
	{
		_, err := NewBIC("DEUTDEFF5001")
		var fe FormatError
		check.Equal(t, errors.As(err, &fe), true)
	}
	{
		_, err := NewBIC("1EUTDEFF500")
		var fe FormatError
		check.Equal(t, errors.As(err, &fe), true)
	}
	check.Equal(t, BIC{}.String(), "")
	{
		_, err := NewBIC("DE123456")
		check.Equal(t, err.Error(), `invalid format: expected upper case alpha character at position 3, got "1"`)
	}
	{
		_, err := NewBIC("DEUTDEFF5001")
		check.Equal(t, err.Error(), `invalid format: expected no more character at position 12, got "1"`)
	}
}

func FuzzNewBIC(f *testing.F) {
	f.Add("DEUTDEFF")
	f.Add("DEUTDEFF500")
	f.Add("dEUTDeFf500")
	f.Add("DEUTDEF")
	f.Add("DEUTDEFF5")
	f.Add("DEUTDEFF5001")
	f.Add("1EUTDEFF500")
	f.Add("DE123456")
	f.Add("DEUTDEFF5001")
	f.Fuzz(func(t *testing.T, s string) {
		bic, err := NewBIC(s)
		if err != nil {
			var ferr FormatError
			var tserr TooShortError
			if errors.As(err, &ferr) || errors.As(err, &tserr) {
				return
			}
			t.Fatal(err)
		}
		bic2, err := NewBIC(bic.String())
		check.Equal(t, nil, err).Fatal()
		check.Equal(t, bic, bic2)
	})
}
