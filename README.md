<!--
SPDX-FileCopyrightText: 2024 Olivier Charvin
SPDX-License-Identifier: CC0-1.0
-->

![Swift Gopher - based on Gopherize.me output - Artwork by Ashley McNamara - inspired by Renee French - Web app by Mat Ryer](./internal/swift-based-on-gopherize.me.png "Swift Gopher - based on Gopherize.me generation - Artwork by Ashley McNamara - inspired by Renee French - Web app by Mat Ryer")

# swift [![Vanitydoc Reference](https://code.pfad.fr/vanitydoc.svg)](https://code.pfad.fr/swift/) [![Pipeline Status](https://ci.codeberg.org/api/badges/13109/status.svg)](https://ci.codeberg.org/repos/13109) [![Go Report Card](https://goreportcard.com/badge/code.pfad.fr/swift)](https://goreportcard.com/report/code.pfad.fr/swift)

Package `code.pfad.fr/swift` provides methods to check offline the validity of an IBAN (and to retrieve some BIC codes). Example and documentation are available on https://code.pfad.fr/swift/ and on [pkg.go.dev](https://pkg.go.dev/code.pfad.fr/swift).

## Updating the registry data

To update the BBAN registry and BIC data, run `make generate`, commit the changes and push a new tag.

## License

European Union Public License 1.2 or later (EUPL-1.2+).

> I consider that importing this library (unmodified) within a larger work (as long as it does more than just validation and IBAN or BIC codes manipulation) not to be a "Derivative Work" (so you don't need to release your work under a EUPL-1.2 compatible license in such a case).
