// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0
module code.pfad.fr/swift

go 1.21

require code.pfad.fr/check v1.0.0-rc.1
