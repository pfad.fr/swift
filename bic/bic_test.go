// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package bic

import (
	"testing"

	"code.pfad.fr/check"
	"code.pfad.fr/swift"
)

func TestFromIBAN(t *testing.T) {
	check.Equal(t, "", FromIBAN(swift.IBAN{
		CountryCode: "",
		BBAN:        "",
	}).String())

	check.Equal(t, "NABAATWWXXX", FromIBAN(swift.IBAN{
		CountryCode: "AT",
		BBAN:        "001000023457201",
	}).String())

	check.Equal(t, "BUNDATWWXXX", FromIBAN(swift.IBAN{
		CountryCode: "AT",
		BBAN:        "01000",
	}).String())

	check.Equal(t, "", FromIBAN(swift.IBAN{
		CountryCode: "AT",
		BBAN:        "",
	}).String())

	check.Equal(t, "", FromIBAN(swift.IBAN{
		CountryCode: "AT",
		BBAN:        "0010",
	}).String())

	check.Equal(t, "GKCCBEBB", FromIBAN(swift.IBAN{
		CountryCode: "BE",
		BBAN:        "096123456769",
	}).String())

	check.Equal(t, "CRESCHZZ80A", FromIBAN(swift.IBAN{
		CountryCode: "CH",
		BBAN:        "04835012345678009",
	}).String())

	check.Equal(t, "NTSBDEB1XXX", FromIBAN(swift.IBAN{
		CountryCode: "DE",
		BBAN:        "10011001123456",
	}).String())
	check.Equal(t, "SOGEDEFFXXX", FromIBAN(swift.IBAN{
		CountryCode: "DE",
		BBAN:        "512108001245126199",
	}).String())

	check.Equal(t, "CAIXESBBXXX", FromIBAN(swift.IBAN{
		CountryCode: "ES",
		BBAN:        "21000813610123456789",
	}).String())

	check.Equal(t, "AGRIFRPPXXX", FromIBAN(swift.IBAN{
		CountryCode: "FR",
		BBAN:        "30006000011234567890189",
	}).String())

	check.Equal(t, "SERBLI22XXX", FromIBAN(swift.IBAN{
		CountryCode: "LI",
		BBAN:        "08806123456789012",
	}).String())

	check.Equal(t, "BCEELULL", FromIBAN(swift.IBAN{
		CountryCode: "LU",
		BBAN:        "0010001234567891",
	}).String())

	check.Equal(t, "CMCIMCM1LYB", FromIBAN(swift.IBAN{
		CountryCode: "MC",
		BBAN:        "10096180790123456789085",
	}).String())

	check.Equal(t, "ABNANL2A", FromIBAN(swift.IBAN{
		CountryCode: "NL",
		BBAN:        "ABNA0123456789",
	}).String())
}

func FuzzFromIBAN(f *testing.F) {
	f.Add("NL", uint8(0), "ABNA0123456789")
	f.Add("MC", uint8(0), "10096180790123456789085")
	f.Add("", uint8(0), "")
	f.Add("AT", uint8(0), "001000023457201")
	f.Add("AT", uint8(0), "")
	f.Fuzz(func(t *testing.T, countryCode string, digits uint8, bban string) {
		iban := swift.IBAN{
			CountryCode: swift.CountryCode(countryCode),
			CheckDigits: digits,
			BBAN:        bban,
		}
		bic := FromIBAN(iban).String()
		if bic == "" {
			return
		}
		b, err := swift.NewBIC(bic)
		check.Equal(t, nil, err).Fatal()
		check.Equal(t, bic, b.String())
	})
}
