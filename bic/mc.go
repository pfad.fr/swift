// SPDX-License-Identifier: EUPL-1.2+
// SPDX-FileCopyrightText: Association Monégasque des Activités Financières (AMAF)
// Source: https://www.amaf.mc/fr/bic-iban
// Code generated by code.pfad.fr/swift/internal/cmd/bic-generate DO NOT EDIT.

package bic

import "code.pfad.fr/swift"

var mcBLZtoBIC = map[string]swift.BIC{
	"1009618079": {"CMCI", "MC", "M1", "LYB"},
	"1009618414": {"CMCI", "MC", "M1", "LYB"},
	"1009618579": {"CMCI", "MC", "M1", "LYB"},
	"1016099001": {"CMMD", "MC", "M1", "XXX"},
	"1149800001": {"BPPB", "MC", "MC", ""},
	"1166840001": {"BERL", "MC", "MC", "XXX"},
	"1199900001": {"UBSW", "MC", "MX", "XXX"},
	"1209804122": {"SDBM", "MC", "M2", ""},
	"1209804123": {"SDBM", "MC", "M2", ""},
	"1209804124": {"SDBM", "MC", "M2", ""},
	"1209804125": {"SDBM", "MC", "M2", ""},
	"1209804126": {"SDBM", "MC", "M2", ""},
	"1209804127": {"SDBM", "MC", "M2", ""},
	"1209804128": {"SDBM", "MC", "M2", ""},
	"1209804133": {"SDBM", "MC", "M2", ""},
	"1209804134": {"SDBM", "MC", "M2", ""},
	"1209804135": {"SDBM", "MC", "M2", ""},
	"1209804136": {"SDBM", "MC", "M2", ""},
	"1209804137": {"SDBM", "MC", "M2", ""},
	"1209804138": {"SDBM", "MC", "M2", ""},
	"1244861017": {"BARC", "MC", "MX", "XXX"},
	"1244861088": {"BARC", "MC", "MX", "XXX"},
	"1244861091": {"BARC", "MC", "MX", "XXX"},
	"1273900070": {"CFMO", "MC", "MX", "XXX"},
	"1273900071": {"CFMO", "MC", "MX", "XXX"},
	"1273900072": {"CFMO", "MC", "MX", "XXX"},
	"1273900073": {"CFMO", "MC", "MX", "XXX"},
	"1273900074": {"CFMO", "MC", "MX", "XXX"},
	"1273900075": {"CFMO", "MC", "MX", "XXX"},
	"1273900076": {"CFMO", "MC", "MX", "XXX"},
	"1273900077": {"CFMO", "MC", "MX", "XXX"},
	"1333800001": {"KBLX", "MC", "MC", "XXX"},
	"1336800001": {"SGBT", "MC", "MC", ""},
	"1336900009": {"MAEA", "MC", "M1", "XXX"},
	"1450800001": {"BAER", "MC", "MC", "XXX"},
	"1460700758": {"CCBP", "MC", "M1", "XXX"},
	"1460700764": {"CCBP", "MC", "M1", "XXX"},
	"1460700765": {"CCBP", "MC", "M1", "XXX"},
	"1460700796": {"CCBP", "MC", "M1", "XXX"},
	"1490800001": {"POSO", "MC", "M1", "XXX"},
	"1563800001": {"BACA", "MC", "MC", "XXX"},
	"1603800001": {"HAVL", "MC", "MX", "XXX"},
	"1664800014": {"UBPG", "MC", "MX", ""},
	"1728800001": {"PICT", "MC", "MC", ""},
	"1756900001": {"CMBM", "MC", "MX", "XXX"},
	"1831520000": {"CEPA", "MC", "M1", ""},
	"1875900001": {"EFGB", "MC", "MC", "XXX"},
	"1910600698": {"AGRI", "MC", "M1", ""},
	"2434900001": {"BJSB", "MC", "MX", "XXX"},
	"3000203214": {"CRLY", "MC", "M1", "XXX"},
	"3000203243": {"CRLY", "MC", "M1", "XXX"},
	"3000203260": {"CRLY", "MC", "M1", "XXX"},
	"3000203290": {"CRLY", "MC", "M1", "XXX"},
	"3000203291": {"CRLY", "MC", "M1", "XXX"},
	"3000205430": {"CRLY", "MC", "M1", "XXX"},
	"3000300909": {"SOGE", "MC", "M1", ""},
	"3000300910": {"SOGE", "MC", "M1", ""},
	"3000300945": {"SOGE", "MC", "M1", ""},
	"3000300952": {"SOGE", "MC", "M1", ""},
	"3000300957": {"SOGE", "MC", "M1", ""},
	"3000301504": {"SOGE", "MC", "M1", ""},
	"3000302308": {"SOGE", "MC", "M1", ""},
	"3000409170": {"BNPA", "MC", "M1", ""},
	"3000409172": {"BNPA", "MC", "M1", ""},
	"3000409174": {"BNPA", "MC", "M1", ""},
	"3000409178": {"BNPA", "MC", "M1", ""},
	"3000409179": {"BNPA", "MC", "M1", ""},
}
