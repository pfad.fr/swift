// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: CC0-1.0

package swift_test

import (
	"fmt"

	"code.pfad.fr/swift"
	"code.pfad.fr/swift/bic"
)

var germanTestIBAN = "DE75512108001245126199"

func Example() {
	iban, err := swift.NewIBAN(germanTestIBAN, swift.CountryBelongsToSEPA())
	if err != nil {
		panic(err)
	}
	fmt.Println("IBAN:", iban.MaskedAndSpaced())
	fmt.Println("BIC: ", bic.FromIBAN(iban).String())
	// Output:
	// IBAN: DE75 512* **** **** ***1 99
	// BIC:  SOGEDEFFXXX
}
