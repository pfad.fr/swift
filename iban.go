// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

// package swift provides methods to check the validity of an IBAN (and retrieve the BIC for some countries).
// Play with the [demo in the browser], powered by JS+Wasm.
//
// [demo in the browser]: https://code.pfad.fr/swift/iban.html
package swift

import (
	"fmt"
	"regexp"
	"strings"

	"code.pfad.fr/swift/internal/format"
)

var nonAlphanumeric = regexp.MustCompile(`[^0-9A-Za-z]`)

// IBANValidator makes additional checks on a given IBAN.
type IBANValidator func(IBAN) error

// NewIBAN sanitizes, parses and checks an IBAN (length, format, check digit sum).
// See [CountryBelongsToIBANRegistry] and [CountryBelongsToSEPA] to perform country code validation.
// See [NationalFormatCheck] to additionnally verify the national format.
func NewIBAN(s string, additionalRules ...IBANValidator) (IBAN, error) {
	s = nonAlphanumeric.ReplaceAllString(s, "")

	// a bit hacky to have a pseudo-fixed bban length between minBBANLength and maxBBANLength
	bbanLen := truncate(minBBANLength, len(s)-4, maxBBANLength)

	cc := columns{
		format.AlphaInsensitive(2), // country code will be uppercased later
		format.Numeric(2),          // check digits
		format.Mixed(bbanLen),
	}
	parts, err := cc.match(s)
	var iban IBAN
	if len(parts) > 0 {
		iban.CountryCode = CountryCode(strings.ToUpper(parts[0])) // uppercase the country code
	}
	if len(parts) > 1 {
		// no error can happen, since the cc.match ensured 2 digits
		iban.CheckDigits, _ = parseUint8(parts[1])
	}
	if len(parts) > 2 {
		iban.BBAN = parts[2]
	}
	if err != nil {
		if e, ok := err.(TooShortError); ok {
			// adjust the length error to match the expected country IBAN length
			expectedLen := bbanRegistryByCountryCode[iban.CountryCode].columns.len()
			if expectedLen > 0 {
				e.ExpectedLen = expectedLen + 4 // account for country+check digits
				err = e
			}
		}
		return iban, err
	}

	// check additionalRule first, since they are probably more important for the end-user
	for _, v := range additionalRules {
		if err := v(iban); err != nil {
			return iban, err
		}
	}

	if cd := iban.checkDigits(); cd != iban.CheckDigits {
		return iban, CheckDigitsError{
			Expected:      iban.CheckDigits,
			Actual:        cd,
			NationalCheck: false,
		}
	}

	return iban, nil
}

func truncate(min, v, max int) int {
	if v < min {
		return min
	}
	if v > max {
		return max
	}
	return v
}

// checkDigitsISO7064 helps computes the modulo of 100*s
func checkDigitsISO7064(s string, modulo uint8) uint8 {
	return uint8(((ibanMod(s, uint32(modulo))) * 100) % uint32(modulo))
}

func ibanMod(s string, modulo uint32) uint32 {
	n := uint32(0)
	for _, c := range s {
		n *= 10
		v := ibanRuneValue(c)
		if v >= 10 {
			n *= 10
		}

		n += v
		// compute modulo at each step to prevent overflow
		// mathematically sound, because (10*x + y) mod z = (10*(x mod z) + y) mod z
		n %= modulo
	}
	return n
}

func (iban IBAN) checkDigits() uint8 {
	s := strings.ToUpper(iban.BBAN) + string(iban.CountryCode)
	return 98 - checkDigitsISO7064(s, 97)
}

func ibanRuneValue(b rune) uint32 {
	if b >= '0' && b <= '9' {
		return uint32(b - '0')
	}
	if b >= 'A' && b <= 'Z' {
		return uint32(b - 'A' + 10)
	}
	// this should never happen, since the regex checked for digits
	panic("invalid rune: " + string(b))
}

// IBAN represents an International Bank Account Number
type IBAN struct {
	CountryCode CountryCode
	CheckDigits uint8
	BBAN        string
}

// String returns the IBAN, without spaces
func (iban IBAN) String() string {
	return fmt.Sprintf("%s%02d%s", iban.CountryCode, iban.CheckDigits, iban.BBAN)
}

// Spaced returns the IBAN with spaces between every 4 characters.
func (iban IBAN) Spaced() string {
	return insertSpaceEvery(4, iban.String())
}

// MaskedAndSpaced returns the spaced IBAN with only the first and last 3 characters
// of the BBAN visible (other chars are replaced with "*").
//
// IBANs are not secret, but it shouldn't hurt to hide parts of them.
// Since BBANs are longer than 11 characters, at least 5 chars will be hidden
// (still partially recoverable using the leading check digits).
// Example:
//
//	DE75 512* **** **** ***1 99
func (iban IBAN) MaskedAndSpaced() string {
	if len(iban.BBAN) < 6 {
		// BBAN is invalid, print without *
		return insertSpaceEvery(4,
			fmt.Sprintf("%s%02d", iban.CountryCode, iban.CheckDigits)+
				iban.BBAN,
		)
	}
	return insertSpaceEvery(4,
		fmt.Sprintf("%s%02d", iban.CountryCode, iban.CheckDigits)+
			iban.BBAN[0:3]+
			strings.Repeat("*", len(iban.BBAN)-6)+
			iban.BBAN[len(iban.BBAN)-3:],
	)
}

func insertSpaceEvery(n int, s string) string {
	var buf strings.Builder
	for i, r := range s {
		if i%n == 0 && i != 0 {
			buf.WriteRune(' ')
		}
		buf.WriteRune(r)
	}
	return buf.String()
}
