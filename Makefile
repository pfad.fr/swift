# SPDX-FileCopyrightText: 2022 Olivier Charvin
#
# SPDX-License-Identifier: CC0-1.0

generate: generate-bic generate-registry

generate-update:
	cd internal/cmd && go get -u ./... code.pfad.fr/swift@v0.8.2 && go mod tidy

generate-bic:
	cd internal/cmd && go run ./bic-generate ../../bic
generate-registry:
	cd internal/cmd && go run ./registry-generate ../..

test:
	@$(MAKE) -s test-go-cover
	@$(MAKE) -s test-license

test-go-cover: test-internal-cmd
	go test -race -cover -coverpkg=./... -coverprofile $(COVERFOLDER)cover.all.out ./...
	@echo "Check if total coverage > 80%"
	@go tool cover -func $(COVERFOLDER)cover.all.out | tail -n1 | tee $(COVERFOLDER)cover.summary.out
	@grep -qE '([89][0-9](\.[0-9])?%)|100\.0%' $(COVERFOLDER)cover.summary.out
	@rm $(COVERFOLDER)cover.*.out

test-internal-cmd:
	cd internal/cmd && go test ./...

test-license:
	reuse lint

fuzz:
	go test -fuzztime=10s -fuzz=NewIBAN
	go test -fuzztime=10s -fuzz=NewBIC
	go test -fuzztime=10s -fuzz=FromIBAN ./bic

commit-and-push:
	git add .
	git commit -m "make generate"
	git push origin HEAD:refs/for/main/main -o topic="make-generate" -o force-push="true" -o title="Update registry data"

tag:
	$(eval VERSION=$(shell git describe --tags --abbrev=0 | awk -F. '{OFS="."; $$NF+=1; print $0}'))
	git tag $(VERSION)
	git push origin $(VERSION)
