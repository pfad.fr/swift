// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"testing"

	"code.pfad.fr/check"
	"code.pfad.fr/swift/internal/format"
)

func TestFormat(t *testing.T) {
	cc := columns{
		format.Numeric(1),
		format.Alpha(2),
		format.Mixed(3),
	}
	parts, err := cc.match("1BC4eF")
	check.Equal(t, err, nil)
	check.EqualSlice(t, []string{"1", "BC", "4eF"}, parts)

	_, err = cc.match("1BC")
	check.Equal(t, err.(TooShortError), TooShortError{
		ActualLen:   3,
		ExpectedLen: 6,
	})

	_, err = cc.match("ABC4eF")
	check.Equal(t, err.(FormatError), FormatError{
		Position:     0,
		Expected:     ExpectedNumericChar,
		Got:          "A",
		Part:         "A",
		PartPosition: 0,
	})

	_, err = cc.match("10C4eF")
	check.Equal(t, err.(FormatError), FormatError{
		Position:     1,
		Expected:     ExpectedUppercaseAlphaChar,
		Got:          "0",
		Part:         "0C",
		PartPosition: 1,
	})

	_, err = cc.match("1BC4e_")
	check.Equal(t, err.(FormatError), FormatError{
		Position:     5,
		Expected:     ExpectedAlphaNumericChar,
		Got:          "_",
		Part:         "4e_",
		PartPosition: 3,
	})

	_, err = cc.match("1BC4eFG+")
	check.Equal(t, err.(FormatError), FormatError{
		Position:     6,
		Expected:     ExpectedNoMoreChar,
		Got:          "G+",
		Part:         "G+",
		PartPosition: 6,
	})
}

func TestFormatError(t *testing.T) {
	check.Equal(t, TooShortError{
		ActualLen:   3,
		ExpectedLen: 6,
	}.Error(), "too short: expected 6 characters, got 3")

	check.Equal(t, FormatError{
		Position:     0,
		Expected:     ExpectedNumericChar,
		Got:          "A",
		Part:         "A",
		PartPosition: 0,
	}.Error(), `invalid format: expected numeric character at position 1, got "A"`)

	check.Equal(t, FormatError{
		Position:     0,
		Expected:     ExpectedAlphaChar,
		Got:          "A",
		Part:         "A",
		PartPosition: 0,
	}.Error(), `invalid format: expected alpha character at position 1, got "A"`)

	check.Equal(t, FormatError{
		Position:     1,
		Expected:     ExpectedUppercaseAlphaChar,
		Got:          "0",
		Part:         "0C",
		PartPosition: 1,
	}.Error(), `invalid format: expected upper case alpha character at position 2, got "0"`)

	check.Equal(t, FormatError{
		Position:     5,
		Expected:     ExpectedAlphaNumericChar,
		Got:          "_",
		Part:         "4e_",
		PartPosition: 3,
	}.Error(), `invalid format: expected alphanumeric character at position 6, got "_"`)

	check.Equal(t, FormatError{
		Position:     6,
		Expected:     ExpectedNoMoreChar,
		Got:          "G+",
		Part:         "G",
		PartPosition: 6,
	}.Error(), `invalid format: expected no more character at position 7, got "G+"`)

	check.Equal(t, CheckDigitsError{
		Expected:      14,
		Actual:        9,
		NationalCheck: false,
	}.Error(), `wrong check digits`)

	check.Equal(t, CheckDigitsError{
		Expected:      14,
		Actual:        19,
		NationalCheck: true,
	}.Error(), `wrong check digits for this country code`)
}
