// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"strconv"
	"strings"
)

// NationalFormatCheck checks the national rules (format and check digits),
// if there is a registered BBANChecker for this country code.
func NationalFormatCheck() IBANValidator {
	return func(iban IBAN) error {
		return bbanRegistryByCountryCode[iban.CountryCode].Check(iban.BBAN)
	}
}

// NOrway has a BBAN length of 11
const (
	minBBANLength = 11
	maxBBANLength = 30
)

type bbanChecker struct {
	check         func(string) error
	columns       columns // will check the length as well
	belongsToSEPA bool
}

func (cc bbanChecker) Check(bban string) error {
	if len(cc.columns) > 0 {
		_, err := cc.columns.match(bban)

		// add 4 to account for the country code + check digits prefix
		switch e := err.(type) {
		case TooShortError:
			return TooShortError{
				ActualLen:   e.ActualLen + 4,
				ExpectedLen: e.ExpectedLen + 4,
			}
		case FormatError:
			return FormatError{
				Position:     e.Position + 4,
				Expected:     e.Expected,
				Got:          e.Got,
				Part:         e.Part,
				PartPosition: e.PartPosition + 4,
			}
		default:
			return err
		case nil:
		}
	}

	// empty value: accept all bban
	if cc.check == nil {
		return nil
	}
	return cc.check(bban)
}

func parseUint8(s string) (uint8, error) {
	d, err := strconv.ParseUint(s, 10, 8)
	return uint8(d), err
}

func mod97Checker(complement uint8, length int) func(string) error {
	return func(s string) error {
		actual := complement - checkDigitsISO7064(s[:length], 97)
		expected, err := parseUint8(s[length:])
		if err != nil {
			return err
		}
		if actual != expected {
			return CheckDigitsError{
				Actual:        actual,
				Expected:      expected,
				NationalCheck: true,
			}
		}
		return nil
	}
}

func beCheckDigits(s string) error {
	actual := uint8(ibanMod(s[:10], 97))
	if actual == 0 {
		actual = 97
	}
	expected, err := parseUint8(s[10:])
	if err != nil {
		return err
	}
	if actual != expected {
		return CheckDigitsError{
			Actual:        actual,
			Expected:      expected,
			NationalCheck: true,
		}
	}
	return nil
}

func frCheckDigits(s string) error {
	// s is ASCII encoded (alphanumeric), so operating on bytes or runes is the same
	buf := []byte(strings.ToUpper(s)[:21])

	// special french mapping
	for i := range buf {
		buf[i] = frLetterValue(buf[i])
	}
	actual := 97 - checkDigitsISO7064(string(buf), 97)

	expected, err := parseUint8(s[21:])
	if err != nil {
		return err
	}
	if actual != expected {
		return CheckDigitsError{
			Actual:        actual,
			Expected:      expected,
			NationalCheck: true,
		}
	}

	return nil
}

func frLetterValue(b byte) byte {
	if b >= 'A' && b <= 'I' {
		return b - 'A' + 1 + '0'
	}
	if b >= 'J' && b <= 'R' {
		return b - 'J' + 1 + '0'
	}
	if b >= 'S' && b <= 'Z' {
		return b - 'S' + 2 + '0'
	}
	return b
}

func hrCheckDigits(s string) error {
	bankDigit := isoIEC7064Mod11_10(s[:7])
	if bankDigit != 9 {
		return CheckDigitsError{
			Actual:        bankDigit,
			Expected:      9,
			NationalCheck: true,
		}
	}
	accountDigit := isoIEC7064Mod11_10(s[7:])
	if accountDigit != 9 {
		return CheckDigitsError{
			Actual:        accountDigit,
			Expected:      9,
			NationalCheck: true,
		}
	}
	return nil
}

// implemented according to https://de.wikipedia.org/wiki/ISO/IEC_7064#Algorithmus_f%C3%BCr_hybride_Systeme
func isoIEC7064Mod11_10(s string) uint8 {
	n := uint32(10)
	for _, c := range s {
		v := (ibanRuneValue(c) + n) % 10
		if v == 0 {
			v = 10
		}
		n = (v * 2) % 11
	}
	n = 11 - n
	if n == 10 {
		return 0
	}
	return uint8(n)
}
