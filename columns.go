// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"fmt"

	"code.pfad.fr/swift/internal/format"
)

type columns []format.Column

func (cc columns) len() int {
	l := 0
	for _, c := range cc {
		l += c.Len()
	}
	return l
}

// match returns the validated parts (might be shorter than expected in case of error)
func (cc columns) match(s string) (parts []string, err error) {
	start := 0
	for _, c := range cc {
		l := c.Len()
		if len(s[start:]) < l {
			return parts, TooShortError{
				ActualLen:   len(s),
				ExpectedLen: cc.len(),
			}
		}
		part := s[start : start+l]
		i := c.IndexInvalid(part)
		if i >= 0 {
			return parts, FormatError{
				Position:     start + i,
				Expected:     c.Expected(),
				Got:          s[start+i : start+i+1],
				Part:         part,
				PartPosition: start,
			}
		}
		parts = append(parts, part)
		start += l
	}
	if len(s) > start {
		return parts, FormatError{
			Position:     start,
			Expected:     ExpectedNoMoreChar,
			Got:          s[start:],
			Part:         s[start:],
			PartPosition: start,
		}
	}
	return parts, nil
}

// TooShortError indicates that the provided string is shorter than expected.
type TooShortError struct {
	ActualLen   int
	ExpectedLen int
}

type ExpectedCharType = format.ExpectedCharType

const (
	ExpectedNoMoreChar         ExpectedCharType = format.NoMoreChar
	ExpectedUppercaseAlphaChar ExpectedCharType = format.UppercaseAlphaChar       // (A-Z)
	ExpectedAlphaChar          ExpectedCharType = format.AlphaChar                // (a-z, A-Z)
	ExpectedNumericChar        ExpectedCharType = format.ExpectedNumericChar      // (0-9)
	ExpectedAlphaNumericChar   ExpectedCharType = format.ExpectedAlphaNumericChar // (a-z, A-Z, 0-9)
)

func (e TooShortError) Error() string {
	return fmt.Sprintf("too short: expected %d characters, got %d", e.ExpectedLen, e.ActualLen)
}

// FormatError indicates that an unexpected character type was encountered.
type FormatError struct {
	Position int              // index in the submitted string where the error occurred
	Expected ExpectedCharType // expected type of character
	Got      string           // submitted character

	Part         string // part of the submitted string considered (all chars should conform to the expected type)
	PartPosition int    // start index of the part above (from the start of the submitted string)
}

func (e FormatError) Error() string {
	// increment the position by 1, since most end-users expect a 1-indexation
	return fmt.Sprintf("invalid format: expected %s at position %d, got %q", e.Expected.String(), e.Position+1, string(e.Got))
}

// CheckDigitsError indicates that the checksum did not match the expected check digits.
type CheckDigitsError struct {
	Expected      uint8
	Actual        uint8
	NationalCheck bool // true if the check failed at the country level (within the BBAN)
}

func (e CheckDigitsError) Error() string {
	// don't display the expected/actual digits, because the error is most likely somewhere else in the BBAN
	s := "wrong check digits"
	if e.NationalCheck {
		s += " for this country code"
	}
	return s
}
