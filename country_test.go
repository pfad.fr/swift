// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import (
	"testing"

	"code.pfad.fr/check"
)

func TestSEPA(t *testing.T) {
	// from https://www.europeanpaymentscouncil.eu/sites/default/files/kb/file/2023-01/EPC409-09%20EPC%20List%20of%20SEPA%20Scheme%20Countries%20v4.0_0.pdf
	sepaCountries := []CountryCode{
		"FI",
		"AD",
		"AT",
		"PT",
		"BE",
		"BG",
		"ES",
		"HR",
		"CY",
		"CZ",
		"DK",
		"EE",
		"FI",
		"FR",
		"FR",
		"DE",
		"GI",
		"GR",
		"FR",
		"GB",
		"HU",
		"IS",
		"IE",
		"GB",
		"IT",
		"GB",
		"LV",
		"LI",
		"LT",
		"LU",
		"PT",
		"MT",
		"FR",
		"FR",
		"MC",
		"NL",
		"NO",
		"PL",
		"PT",
		"FR",
		"RO",
		"FR",
		"FR",
		"FR",
		"SM",
		"SK",
		"SI",
		"ES",
		"SE",
		"CH",
		"GB",
		"VA",
	}
	distinctCountries := make(map[CountryCode]bool)
	for _, c := range sepaCountries {
		check.Equal(t, c.BelongsToSEPA(), true)
		distinctCountries[c] = true
	}
	check.Equal(t, CountryCode("KW").BelongsToSEPA(), false) // Koweit

	bbanRegistryCount := 0
	for _, bc := range bbanRegistryByCountryCode {
		if bc.belongsToSEPA {
			bbanRegistryCount++
		}
	}
	check.Equal(t, bbanRegistryCount, len(distinctCountries))
}
