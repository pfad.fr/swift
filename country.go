// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift

import "errors"

// CountryCode is the ISO 3166-1 alpha-2 code of the country (uppercase)
type CountryCode string

// BelongsToSEPA indicates if a country belongs to the Single Euro Payments Area
// according to https://www.swift.com/resource/iban-registry-pdf
func (cc CountryCode) BelongsToSEPA() bool {
	return bbanRegistryByCountryCode[cc].belongsToSEPA
}

// BelongsToIBANRegistry returns true if the country is listed in the iban registry
// https://www.swift.com/resource/iban-registry-pdf
func (cc CountryCode) BelongsToIBANRegistry() bool {
	_, ok := bbanRegistryByCountryCode[cc]
	return ok
}

// ErrCountryNotInRegistry is returned when a country is not listed in the iban registry.
var ErrCountryNotInRegistry = errors.New("country is not listed in the IBAN registry")

// CountryBelongsToIBANRegistry ensures that the country is listed in the iban registry
// https://www.swift.com/resource/iban-registry-pdf
func CountryBelongsToIBANRegistry() IBANValidator {
	return func(iban IBAN) error {
		if !iban.CountryCode.BelongsToIBANRegistry() {
			return ErrCountryNotInRegistry
		}
		return nil
	}
}

// ErrCountryOutsideSEPA is returned when a country does not belong to the Single Euro Payments Area.
var ErrCountryOutsideSEPA = errors.New("country is outside the Single Euro Payments Area (SEPA)")

// CountryBelongsToSEPA ensures that the country belongs to the Single Euro Payments Area
// according to https://www.swift.com/resource/iban-registry-pdf
func CountryBelongsToSEPA() IBANValidator {
	return func(iban IBAN) error {
		if !iban.CountryCode.BelongsToSEPA() {
			return ErrCountryOutsideSEPA
		}
		return nil
	}
}
