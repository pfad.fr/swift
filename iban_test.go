// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: EUPL-1.2+

package swift_test

import (
	"errors"
	"testing"

	"code.pfad.fr/check"
	"code.pfad.fr/swift"
)

func validIBAN(t testing.TB, input string) string {
	t.Helper()
	iban, err := swift.NewIBAN(input, swift.NationalFormatCheck(), swift.CountryBelongsToIBANRegistry())
	check.Equal(t, err, nil)
	return iban.String()
}

func TestParseValidIBAN(t *testing.T) {
	serialized := validIBAN(t, " GB82 WEST 1234 5698 7654 32 	")
	check.Equal(t, serialized, "GB82WEST12345698765432")

	serialized = validIBAN(t, "IE06 BOFI 9000 0112 3907 09")
	check.Equal(t, serialized, "IE06BOFI90000112390709")

	serialized = validIBAN(t, "DE68210501700012345678")
	check.Equal(t, serialized, "DE68210501700012345678")

	serialized = validIBAN(t, "FR7420041010050500013 AM 2646")
	check.Equal(t, serialized, "FR7420041010050500013AM2646")

	serialized = validIBAN(t, "FR7420041010050500013 Am 2646")
	check.Equal(t, serialized, "FR7420041010050500013Am2646")

	serialized = validIBAN(t, "FR7630001007941234567890185")
	check.Equal(t, serialized, "FR7630001007941234567890185")

	serialized = validIBAN(t, "MT84MALT011000012345MTLCAST001S")
	check.Equal(t, serialized, "MT84MALT011000012345MTLCAST001S")

	serialized = validIBAN(t, "mt84MALT011000012345mtlcast001s")
	check.Equal(t, serialized, "MT84MALT011000012345mtlcast001s") // IBAN validation happens on uppercase, but lowercase BBAN is allowed

	validIBAN(t, "AD1400080001001234567890")
	validIBAN(t, "AE460090000000123456789")
	validIBAN(t, "AL35202111090000000001234567")
	validIBAN(t, "AT483200000012345864")
	validIBAN(t, "AZ77VTBA00000000001234567890")
	validIBAN(t, "BA393385804800211234")
	validIBAN(t, "BE71096123456769")
	validIBAN(t, "BG18RZBB91550123456789")
	validIBAN(t, "BH02CITI00001077181611")
	validIBAN(t, "BI4210000100010000332045181")
	validIBAN(t, "BR1500000000000010932840814P2")
	validIBAN(t, "BY86AKBB10100000002966000000")
	validIBAN(t, "CH5604835012345678009")
	validIBAN(t, "CR23015108410026012345")
	validIBAN(t, "CY21002001950000357001234567")
	validIBAN(t, "CZ5508000000001234567899")
	validIBAN(t, "DE75512108001245126199")
	validIBAN(t, "DJ2110002010010409943020008")
	validIBAN(t, "DK9520000123456789")
	validIBAN(t, "DO22ACAU00000000000123456789")
	validIBAN(t, "EE471000001020145685")
	validIBAN(t, "EG800002000156789012345180002")
	validIBAN(t, "ES7921000813610123456789")
	validIBAN(t, "FI1410093000123458")
	validIBAN(t, "FO9264600123456789")
	validIBAN(t, "FR7630006000011234567890189")
	validIBAN(t, "GB33BUKB20201555555555")
	validIBAN(t, "GE60NB0000000123456789")
	validIBAN(t, "GI56XAPO000001234567890")
	validIBAN(t, "GL8964710123456789")
	validIBAN(t, "GR9608100010000001234567890")
	validIBAN(t, "GT20AGRO00000000001234567890")
	validIBAN(t, "HR1723600001101234565")
	validIBAN(t, "HR1210010051863000160")
	validIBAN(t, "HU93116000060000000012345676")
	validIBAN(t, "IE64IRCE92050112345678")
	validIBAN(t, "IL170108000000012612345")
	validIBAN(t, "IQ20CBIQ861800101010500")
	validIBAN(t, "IS750001121234563108962099")
	validIBAN(t, "IT60X0542811101000000123456")
	validIBAN(t, "JO71CBJO0000000000001234567890")
	validIBAN(t, "KW81CBKU0000000000001234560101")
	validIBAN(t, "KZ244350000012344567")
	validIBAN(t, "LB92000700000000123123456123")
	validIBAN(t, "LC14BOSL123456789012345678901234")
	validIBAN(t, "LI7408806123456789012")
	validIBAN(t, "LT601010012345678901")
	validIBAN(t, "LU120010001234567891")
	validIBAN(t, "LV97HABA0012345678910")
	validIBAN(t, "LY38021001000000123456789")
	validIBAN(t, "MC5810096180790123456789085")
	validIBAN(t, "MD21EX000000000001234567")
	validIBAN(t, "ME25505000012345678951")
	validIBAN(t, "MK07200002785123453")
	validIBAN(t, "MR1300020001010000123456753")
	validIBAN(t, "MT31MALT01100000000000000000123")
	validIBAN(t, "MU43BOMM0101123456789101000MUR")
	validIBAN(t, "NL02ABNA0123456789")
	validIBAN(t, "NO8330001234567")
	validIBAN(t, "PK36SCBL0000001123456702")
	validIBAN(t, "PL10105000997603123456789123")
	validIBAN(t, "PS92PALS000000000400123456702")
	validIBAN(t, "PT50002700000001234567833")
	validIBAN(t, "QA54QNBA000000000000693123456")
	validIBAN(t, "RO66BACX0000001234567890")
	validIBAN(t, "RS35105008123123123173")
	validIBAN(t, "RU0204452560040702810412345678901")
	validIBAN(t, "SA4420000001234567891234")
	validIBAN(t, "SC74MCBL01031234567890123456USD")
	validIBAN(t, "SD8811123456789012")
	validIBAN(t, "SE7280000810340009783242")
	validIBAN(t, "SI56192001234567892")
	validIBAN(t, "SK8975000000000012345671")
	validIBAN(t, "SM76P0854009812123456789123")
	validIBAN(t, "SO061000001123123456789")
	validIBAN(t, "ST23000200000289355710148")
	validIBAN(t, "SV43ACAT00000000000000123123")
	validIBAN(t, "TL380010012345678910106")
	validIBAN(t, "TN5904018104004942712345")
	validIBAN(t, "TR320010009999901234567890")
	validIBAN(t, "UA903052992990004149123456789")
	validIBAN(t, "VA59001123000012345678")
	validIBAN(t, "VG07ABVI0000000123456789")
	validIBAN(t, "XK051212012345678906")
}

func TestParseInvalidIBAN(t *testing.T) {
	{
		_, err := swift.NewIBAN("")
		check.Equal(t, err.(swift.TooShortError), swift.TooShortError{
			ActualLen:   0,
			ExpectedLen: 15,
		})
	}
	{
		_, err := swift.NewIBAN("XX001234567890", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.TooShortError), swift.TooShortError{
			ActualLen:   14,
			ExpectedLen: 15,
		})
	}
	{
		_, err := swift.NewIBAN("FR001234567890", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.TooShortError), swift.TooShortError{
			ActualLen:   14,
			ExpectedLen: 27, // adjusted for this country
		})
	}
	{
		_, err := swift.NewIBAN("IE11 BOFI 9000 0112 3907 09", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.CheckDigitsError), swift.CheckDigitsError{
			Actual:        6,
			Expected:      11,
			NationalCheck: false,
		})
	}
	{
		_, err := swift.NewIBAN("XX881234567890123456789012345678901", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.FormatError), swift.FormatError{
			Position:     34,
			Expected:     swift.ExpectedNoMoreChar,
			Got:          "1",
			Part:         "1",
			PartPosition: 34,
		})
	}
	{
		_, err := swift.NewIBAN("DE7721050170001234", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.TooShortError), swift.TooShortError{
			ActualLen:   18,
			ExpectedLen: 22,
		})
	}
	{
		_, err := swift.NewIBAN("DE1421050170001234567 A", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.FormatError), swift.FormatError{
			Position:     21,
			Expected:     swift.ExpectedNumericChar,
			Got:          "A",
			Part:         "001234567A",
			PartPosition: 12,
		})
	}
	{
		_, err := swift.NewIBAN("FR26 12345123451234567891Z 16", swift.NationalFormatCheck())
		check.Equal(t, err.(swift.CheckDigitsError), swift.CheckDigitsError{
			Actual:        89,
			Expected:      16,
			NationalCheck: true,
		})
	}
}

func maskedIBAN(t *testing.T, input string) string {
	t.Helper()
	return swift.IBAN{
		CountryCode: swift.CountryCode(input[:2]),
		CheckDigits: 0,
		BBAN:        input[4:],
	}.MaskedAndSpaced()
}

func TestMasked(t *testing.T) {
	check.Equal(t, maskedIBAN(t, "DE0012341234"), "DE00 123* *234")
	check.Equal(t, maskedIBAN(t, "DE00123412345"), "DE00 123* **34 5")
	check.Equal(t, maskedIBAN(t, "DE00123412341234"), "DE00 123* **** *234")
	check.Equal(t, maskedIBAN(t, "DE001"), "DE00 1")
	check.Equal(t, swift.IBAN{
		CountryCode: swift.CountryCode("DE"),
		CheckDigits: 0,
		BBAN:        "",
	}.MaskedAndSpaced(), "DE00")
	check.Equal(t, swift.IBAN{
		CountryCode: swift.CountryCode("X"),
		CheckDigits: 0,
		BBAN:        "",
	}.MaskedAndSpaced(), "X00")
}

func TestIncompleteIBAN(t *testing.T) {
	check := func(t *testing.T, in string, expected swift.IBAN) {
		t.Run(in, func(t *testing.T) {
			actual, err := swift.NewIBAN(in)
			if err == nil {
				t.Fatal("err should not be nil")
			}
			check.Equal(t, actual, expected)
		})
	}
	check(t, "", swift.IBAN{})
	check(t, "X", swift.IBAN{})
	check(t, "DE", swift.IBAN{"DE", 0, ""})
	check(t, "1", swift.IBAN{})
	check(t, "12", swift.IBAN{})
	check(t, "1D", swift.IBAN{})
	check(t, "DE1", swift.IBAN{"DE", 0, ""})
	check(t, "DE01", swift.IBAN{"DE", 1, ""})
	check(t, "DE12", swift.IBAN{"DE", 12, ""})
	check(t, "DE123", swift.IBAN{"DE", 12, ""})
	check(t, "DEX", swift.IBAN{"DE", 0, ""})
}

func spacedIBAN(t *testing.T, input string) string {
	t.Helper()
	return swift.IBAN{
		CountryCode: swift.CountryCode(input[:2]),
		CheckDigits: 0,
		BBAN:        input[4:],
	}.Spaced()
}

func TestSpaced(t *testing.T) {
	check.Equal(t, spacedIBAN(t, "DE0012341234"), "DE00 1234 1234")
	check.Equal(t, spacedIBAN(t, "DE00123412345"), "DE00 1234 1234 5")
	check.Equal(t, spacedIBAN(t, "DE00123412341234"), "DE00 1234 1234 1234")
}

func TestAdditionalRules(t *testing.T) {
	{
		_, err := swift.NewIBAN("FO9264600123456789", swift.CountryBelongsToSEPA())
		check.Equal(t, err, swift.ErrCountryOutsideSEPA)
	}
	{
		const ibanFakeCountry = "XX9764600123456789"
		_, err := swift.NewIBAN(ibanFakeCountry, swift.CountryBelongsToIBANRegistry())
		check.Equal(t, err, swift.ErrCountryNotInRegistry)

		_, err = swift.NewIBAN(ibanFakeCountry)
		check.Equal(t, err, nil)
	}
}

func FuzzNewIBAN(f *testing.F) {
	f.Add("")
	f.Add("XX00123456789")
	f.Add("IE11 BOFI 9000 0112 3907 09")
	f.Add("XX881234567890123456789012345678901")
	f.Add("DE7721050170001234")
	f.Add("DE1421050170001234567 A")
	f.Add("FR26 12345123451234567891Z 16")
	f.Fuzz(func(t *testing.T, s string) {
		iban, err := swift.NewIBAN(s)
		if err != nil {
			var ferr swift.FormatError
			var tserr swift.TooShortError
			var cderr swift.CheckDigitsError
			if errors.As(err, &ferr) || errors.As(err, &tserr) || errors.As(err, &cderr) {
				return
			}
			t.Fatal(err)
		}
		iban2, err := swift.NewIBAN(iban.String())
		check.Equal(t, nil, err).Fatal()
		check.Equal(t, iban2, iban)
	})
}
